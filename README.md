# PDF document sort tool

This simple tool is designed to rename and classify a PDF document according to its content.

The actual tool functionalities:
* Read PDF content (bitmap content not supported yet)
* Collect data from document and store it into a hash map
* Define a new name from previously collected data

## Installation

If you use an Ubuntu system, you can use the package from the last release. Otherwise, you can install this tool using `python setup.py install` command

## Configuration file
An example of configuration file is available [here](config-example.yml)

The configuration file can be:
* passed as an argument (option `-c my_config_file.yml`)
* stored in the `~/.config/pdfdocsort/config.yml`
* located in the current execution directory (`config.yml` named file)

This tool will process each document using this process:
1. Search the suitable rule from the rules in this configuration file using `math` rule attribute
2. Build the data hash map using regular expression patterns from the `data_collector` elements
3. Build a new name following the `name_generator` elements

To do:
* Move the file into the suitable place
* Run as a service and monitor an input directory

### Collected data

Collected data can be:
* a character string
* a date
* a float number

To do: support other data types if needed

### Name builder

Each rule must content a `name_generator` property containing an array of:
* simple character string
* a data (from the previously collected data)
