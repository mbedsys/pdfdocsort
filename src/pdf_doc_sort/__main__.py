#!/usr/bin/env python3

import argparse
import sys

from . import VERSION
from .fileclassifier import FileClassifier


def cli():
    """Main function"""
    parser = argparse.ArgumentParser(
        description="PDF document sort tool version " + VERSION
    )
    parser.add_argument("-c", "--config", help="Configuration file")
    parser.add_argument("files", type=str, nargs="+", help="Input files")
    parser.add_argument(
        "-L",
        "--pdf2text-legacy",
        dest="pdf2text_legacy",
        action=argparse.BooleanOptionalAction,
        help="Use legacy PDF to TEXT extraction method",
    )
    args = parser.parse_args()
    file_classifier = FileClassifier(
        config_path=args.config, pdf2text_legacy=args.pdf2text_legacy
    )
    for pattern in args.files:
        file_classifier.process(pattern)


if __name__ == "__main__":
    cli()
