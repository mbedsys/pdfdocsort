#!/usr/bin/env python3

import subprocess
import coloredlogs, logging
from datetime import datetime
from datetime import date
from dateutil.parser import parse as date_parse
import fitz
import locale
import os
from os.path import expanduser
from pathlib import Path
import pathlib
import re
import yaml


class Error(Exception):
    """Error message"""


def get_node_elt(node, prop, type, default=None):
    """Get a node element"""
    if prop not in node:
        if default is not None:
            return default
        raise Error(
            f"Missing [{prop}] property in the config file")
    if not isinstance(node[prop], type):
        raise Error(
            f"[{prop}] property must be a {type} in the config file"
        )
    return node[prop]


def update_dict(dst, src):
    """Update a dict with an other"""
    for key, value in src.items():
        if isinstance(value, dict):
            dst[key] = update_dict(dst.get(key, {}), value)
        else:
            dst[key] = value
    return dst


class FileClassifier:
    """File classifier"""

    def get_rule(self, text):
        """Get the suitable rule matching with the current document"""
        if "rules" not in self.config or not isinstance(self.config["rules"], list):
            raise Error("Missing [rules] list in the config file")
        for rule in self.config["rules"]:
            if re.search(get_node_elt(rule, "match", str), text):
                return rule

    def compute_data(self, rule, text):
        """Compute data from the document text"""
        data = {}
        for elt in get_node_elt(rule, "data_collector", list):
            try:
                data_name = get_node_elt(elt, "name", str)
                data_type = get_node_elt(elt, "type", str, "auto")
                if "value" in elt:
                    value = elt["value"]
                else:
                    pattern = get_node_elt(elt, "pattern", str)
                    match = re.search(pattern, text)
                    if not match:
                        raise Error(f"Unable to find: {pattern}")
                    value = match.expand(get_node_elt(elt, "template", str))
                if data_type == "date":
                    date_format = get_node_elt(elt, "format", str, "auto")
                    locale.setlocale(
                        locale.LC_ALL, get_node_elt(elt, "locale", str, self.locale)
                    )
                    if date_format == "auto":
                        value = date_parse(value)
                    else:
                        value = datetime.strptime(
                            value, get_node_elt(elt, "format", str)
                        )
                elif data_type == "float":
                    value = float(re.sub(r"[^0-9\.]+", "", value))
                data[data_name] = value
            except Exception as err:
                raise Error(
                    f"Unable to process data element: {str(elt)}{os.linesep}{os.linesep}Raison:{os.linesep}{str(err)}"
                )
        return data

    def compute_name(self, rule, data):
        """Compute the new file name file"""
        name_parts = []
        name_fmt_allowed = (
            rf"[^{get_node_elt(rule, 'name_fmt_allowed', str, self.name_fmt_allowed)}]"
        )
        name_fmt_replacement = get_node_elt(
            rule, "name_fmt_replacement", str, self.name_fmt_replacement
        )
        for part in get_node_elt(rule, "name_generator", list):
            try:
                if isinstance(part, str):
                    name_parts.append(part)
                    continue
                data_name = get_node_elt(part, "data", str)
                if data_name not in data:
                    raise Error(f"Undefined data name [{data_name}]")
                value = data[data_name]
                if isinstance(value, date):
                    value = value.strftime(get_node_elt(part, "format", str, "%Y%m%d"))
                if isinstance(value, float):
                    value = get_node_elt(part, "format", str, "{:.2f}").format(value)
                if not isinstance(value, str):
                    raise Error(f"Unmanaged data [{data_name}]: {str(value)}")
                name_parts.append(re.sub(name_fmt_allowed, name_fmt_replacement, value))
            except Exception as err:
                raise Error(
                    f"Unable to process name generator element: {str(rule)}{os.linesep}{os.linesep}Raison:{os.linesep}{str(err)}"
                )
        return "".join(name_parts)

    def process_file(self, path):
        """Process file"""
        self.logger.debug(f"Process file [{path}]")
        try:
            if self.__pdf2text_legacy:
                with fitz.open(path) as doc:
                    pdf_text = ""
                    for page in doc:
                        pdf_text += page.get_text()
            else:
                process = subprocess.run(
                    ["pdftotext", "-layout", path, "-"],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )
                if process.returncode != 0:
                    raise Error(
                        f"Unable to extract text from the PDF file: {process.stderr.decode()}"
                    )
                pdf_text = process.stdout.decode()
            rule = self.get_rule(pdf_text)
            if rule is None:
                self.logger.error(f"No rule found for the file [{path}]!")
                self.logger.debug(pdf_text)
                return
            if "extends" in rule:
                template_name = get_node_elt(rule, "extends", str)
                templates = get_node_elt(self.config, "templates", dict)
                rule = update_dict(get_node_elt(templates, template_name, dict), rule)
            if get_node_elt(rule, "debug", bool, self.debug):
                self.logger.debug(pdf_text)
            self.logger.debug(f"Found rule: {get_node_elt(rule, 'label', str)}")
            data = self.compute_data(rule, pdf_text)
            file_name = self.compute_name(rule, data)
            new_path = Path(os.path.dirname(path)) / file_name
            if path != new_path and not get_node_elt(
                rule, "dry_run", bool, self.dry_run
            ):
                os.rename(path, new_path)
            self.logger.info(f"[{file_name}] file successfully classified")
        except Error as err:
            self.logger.error(f"Unable to process [{path}] file!")
            self.logger.error("")
            self.logger.error("Raison:")
            for line in str(err).splitlines():
                self.logger.error(line)

    def process(self, pattern):
        """Process files"""
        if pattern[0] == "/":
            base_path = "/"
            pattern = pattern[1:]
        elif pattern[0] == "~":
            path_parts = pattern.split("/")
            base_path = expanduser(path_parts[0])
            pattern = "/".join(path_parts[1:])
        else:
            base_path = os.getcwd()
        for file in pathlib.Path(base_path).glob(pattern):
            self.process_file(str(file))

    def __init__(self, config_path: str, pdf2text_legacy: False):
        """Read config file"""
        self.logger = logging.getLogger(__name__)
        coloredlogs.install(level="INFO", logger=self.logger)
        if config_path is None:
            if "PDS_CONFIG_PATH" in os.environ:
                config_path = Path(os.environ.get("PDS_CONFIG_PATH"))
            elif (Path(os.getcwd()) / "config.yml").exists():
                config_path = Path(os.getcwd()) / "config.yml"
            elif (Path(expanduser("~")) / ".config/pdfdocsort/config.yml").exists:
                config_path = Path(expanduser("~")) / ".config/pdfdocsort/config.yml"
            elif (
                Path(os.path.dirname(os.path.realpath(__file__))) / "config.yml"
            ).exists():
                config_path = (
                    Path(os.path.dirname(os.path.realpath(__file__))) / "config.yml"
                )
            else:
                raise Error("No config file defined!")
        try:
            file_d = open(config_path)
            self.config = yaml.safe_load(file_d.read())
            coloredlogs.install(
                level=get_node_elt(self.config, "log_level", str, "INFO"),
                logger=self.logger,
            )
            defaults = get_node_elt(self.config, "defaults", dict, {})
            self.debug = get_node_elt(defaults, "debug", bool, False)
            self.dry_run = get_node_elt(defaults, "dry_run", bool, False)
            self.locale = get_node_elt(defaults, "locale", str, "en_US.utf8")
            self.name_fmt_allowed = get_node_elt(
                defaults, "name_fmt_allowed", str, "a-zA-Z0-9."
            )
            self.name_fmt_replacement = get_node_elt(
                defaults, "name_fmt_replacement", str, "a-zA-Z0-9."
            )
            if pdf2text_legacy or get_node_elt(
                    defaults, "pdf2text_legacy", bool, True
                ):
                self.__pdf2text_legacy = False
            else:
                self.__pdf2text_legacy = True
        except Exception as err:
            raise Error(
                f"Unable to load [{config_path}] configuration file!{os.linesep}{os.linesep}Raison:{os.linesep}{str(err)}"
            )
